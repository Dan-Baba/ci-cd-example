[![pipeline status](https://gitlab.com/Dan-Baba/ci-cd-example/badges/master/pipeline.svg)](https://gitlab.com/Dan-Baba/ci-cd-example/commits/master)
[![coverage report](https://gitlab.com/Dan-Baba/ci-cd-example/badges/master/coverage.svg)](https://gitlab.com/Dan-Baba/ci-cd-example/commits/master)

# CI CD Example Project
## Purpose
This project is meant to demonstrate how to setup a project with a CI/CD pipeline. This project uses Angular as a web framework, and Node to serve the angular files.

## Features
### Stages and Jobs
Multiple jobs in the testing stage for concurrently running both Angular and Node tests.

### Caching Dependencies
We cache dependencies so building in the future, and future stages don't take as long.
