const express = require('express');
const app = express();
const path = require('path');
const port = process.env.PORT || 4200;
const http = require('http');

// Hello routes
app.use('/api', require('./routes/hello'));

// Routes for serving static files
const allowedExt = [
  '.js',
  '.ico',
  '.css',
  '.png',
  '.jpg',
  '.woff2',
  '.woff',
  '.ttf',
  '.svg',
];

app.get('*', (req, res) => {
  if(allowedExt.filter(ext => req.url.indexOf(ext) > 0).length > 0) {
    res.sendFile(path.resolve('web/' + req.url));
  } else {
    res.sendFile(path.join(__dirname, 'web/index.html'));
  }
});

// App is ready, now we listen for requests.
http.createServer(app).listen(port, () => console.log(`App is listening on port ${port}!`));